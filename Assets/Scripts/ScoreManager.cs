﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public static int puntuacio;
    public string keyPuntuacio = "puntuacio total";
    public static int puntuacioReset;
    Text score;

    // Start is called before the first frame update
    void Start() {

        puntuacio = PlayerPrefs.GetInt(keyPuntuacio);
        score = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        
        PlayerPrefs.SetInt(keyPuntuacio, puntuacio);

        if (gameObject.name.Equals("NumPuntuacioText")){
            score.text = "" + puntuacioReset;
        }else{
            score.text = puntuacio + " punts";
        }
    }
}