﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    List<String> scenesLvl1 = new List<String>{"Mates1","Engranatges1","ComCon 1","Engranatges1","Mates1","ComCon 1","Mates1","Engranatges1","ComCon 1","Mates1"};

    public void PlayDiari (){

        ScoreManager.puntuacioReset = 0;
        int randomNum = UnityEngine.Random.Range(0,9);
        Debug.Log(randomNum);
        string primeraScene = scenesLvl1[randomNum];

        Answer.diari = true;
        Answer.contadorLvl = 1;
        SceneManager.LoadScene(primeraScene);
    }
}
