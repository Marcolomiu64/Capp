﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMenu : MonoBehaviour
{
    public void TornarAlMenu (){

        Answer.diari = false;
        AnswerInfinit.infinit = false;
        SceneManager.LoadScene(0);
    }
}
