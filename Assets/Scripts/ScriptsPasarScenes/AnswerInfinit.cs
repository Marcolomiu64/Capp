﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class AnswerInfinit : MonoBehaviour {
    
    public static bool infinit = false;

    private int contador;
    private string keyNivellsMateixaDificultat = "mateixa dificultat";

    private int contadorDificultat;
    private string keyDificultat = "dificultat infinit";

    private int contadorVides;
    private string keyVides = "vides restants";
    
    List<String> scenesDificultat1 = new List<String>{"Mates1","Engranatges1","ComCon 1","Engranatges1","Mates1","ComCon 1","Mates1","Engranatges1","ComCon 1","Mates1"};
    List<String> scenesDificultat2 = new List<String>{"Siluetes 1","Engranatges2","ComCon 2","Engranatges2","Mates2","ComCon 2","Mates2","Engranatges2","Siluetes 1","Mates2"};
    List<String> scenesDificultat3 = new List<String>{"Mates3","Engranatges3","ComCon 3","Engranatges3","Mates3","ComCon 3","Mates3","Engranatges3","ComCon 3","Mates3"};
    List<String> scenesDificultat4 = new List<String>{"Siluetes 2","Engranatges4","ComCon 4","Engranatges5","Mates4","ComCon 4","Engranatges4","Engranatges5","Siluetes 2","Mates4"};
    List<String> scenesDificultat5 = new List<String>{"Mates5","Engranatges6","ComCon 5","Engranatges7","Mates5","ComCon 5","Engranatges6","Engranatges7","ComCon 5","Mates5"};
    List<String> scenesDificultat6 = new List<String>{"Siluetes 3","Engranatges8","ComCon 6","Engranatges9","Mates6","ComCon 6","Engranatges8","Engranatges9","Siluetes 3","Mates6"};
    List<String> scenesDificultat7 = new List<String>{"Mates7","Engranatges10","ComCon 7","Engranatge11","Mates7","ComCon 7","Engranatges10","Engranatges11","ComCon7","Mates7"};
    List<String> scenesDificultat8 = new List<String>{"Siluetes 4","Engranatges12","ComCon 8","Engranatges13","Mates8","ComCon 8","Engranatges13","Engranatges112","Siluetes 4","Mates8"};
    List<String> scenesDificultat9 = new List<String>{"Mates9","Engranatges14","ComCon 9","Engranatges15","Mates9","ComCon 9","Engranatges14","Engranatges15","ComCon9","Mates9"};
    List<String> scenesDificultat10 = new List<String>{"Mates10","Engranatges16","ComCon 10","Engranatges16","Mates10","ComCon 10","Mates10","Engranatges16","ComCon 10","Mates10"};

    void Start() {
        
        contadorDificultat = PlayerPrefs.GetInt(keyDificultat);
        contadorVides = PlayerPrefs.GetInt(keyVides);        
        contador = PlayerPrefs.GetInt(keyNivellsMateixaDificultat);        
    }

    void Update() {

        PlayerPrefs.SetInt(keyDificultat, contadorDificultat);
        PlayerPrefs.SetInt(keyVides, contadorVides);
        PlayerPrefs.SetInt(keyNivellsMateixaDificultat, contador);
    }

    public void CorrectAnswer (){
        if(infinit == true){
            ScoreManager.puntuacio += 200;
            ScoreManager.puntuacioReset += 200;
            contador++;

            Debug.Log("Acierto");
            Debug.Log("Vides " + contadorVides);
            Debug.Log("Contador " + contador);
            Debug.Log("ContadorDificultat " + contadorDificultat);
            Debug.Log("");

            if(contador == 5){
                contadorDificultat++;
                contador = 0;
            }

            if (contadorDificultat == 11){
                SceneManager.LoadScene("VictoriaInfinit");
            } 

            PassarEscenaInfinit();
        }
    }

    public void IncorrectAnswer (){
        if(infinit == true){
            contadorVides--;
            contador = 0;

            Debug.Log("Fallo");
            Debug.Log("Vides " + contadorVides);
            Debug.Log("Contador " + contador);
            Debug.Log("");

            if(contadorVides == 0){
                contadorVides = 3;
                contadorDificultat = 1;
                SceneManager.LoadScene("FinalInfinit");
            }else{
                PassarEscenaInfinit();              
            }
        }
    }

    public void PassarEscenaInfinit (){

        int randomNum;

        if(contadorDificultat == 1){
            randomNum = UnityEngine.Random.Range(0,scenesDificultat1.Count);
            //Debug.Log(1);
            string scene = scenesDificultat1[randomNum];
            SceneManager.LoadScene(scene);
        }

        if(contadorDificultat == 2){
            randomNum = UnityEngine.Random.Range(0,scenesDificultat2.Count);
            //Debug.Log(2);
            string scene = scenesDificultat2[randomNum];
            SceneManager.LoadScene(scene);
        }

        if(contadorDificultat == 3){
            randomNum = UnityEngine.Random.Range(0,scenesDificultat3.Count);
            //Debug.Log(3);
            string scene = scenesDificultat3[randomNum];
            SceneManager.LoadScene(scene);
        }

        if(contadorDificultat == 4){
            randomNum = UnityEngine.Random.Range(0,scenesDificultat4.Count);
            //Debug.Log(4);
            string scene = scenesDificultat4[randomNum];
            SceneManager.LoadScene(scene);
        }

        if(contadorDificultat == 5){
            randomNum = UnityEngine.Random.Range(0,scenesDificultat5.Count);
            //Debug.Log(5);
            string scene = scenesDificultat5[randomNum];
            SceneManager.LoadScene(scene);
        }

        if(contadorDificultat == 6){
            randomNum = UnityEngine.Random.Range(0,scenesDificultat6.Count);
            //Debug.Log(6);
            string scene = scenesDificultat6[randomNum];
            SceneManager.LoadScene(scene);
        }

        if(contadorDificultat == 7){
            randomNum = UnityEngine.Random.Range(0,scenesDificultat7.Count);
            //Debug.Log(7);
            string scene = scenesDificultat7[randomNum];
            SceneManager.LoadScene(scene);
        }

        if(contadorDificultat == 8){
            randomNum = UnityEngine.Random.Range(0,scenesDificultat8.Count);
            //Debug.Log(8);
            string scene = scenesDificultat8[randomNum];
            SceneManager.LoadScene(scene);
        }

        if(contadorDificultat == 9){
            randomNum = UnityEngine.Random.Range(0,scenesDificultat9.Count);
            //Debug.Log(9);
            string scene = scenesDificultat9[randomNum];
            SceneManager.LoadScene(scene);
        }
        
        if(contadorDificultat == 10){
            randomNum = UnityEngine.Random.Range(0,scenesDificultat10.Count);
            //Debug.Log(10);
            string scene = scenesDificultat10[randomNum];
            SceneManager.LoadScene(scene);
        }


        //Encara s'ha de fer
        
        /*
        for(int i = 0; i<llistaInfinit.Count; i++){
            
            
            
            int randomScene = UnityEngine.Random.Range(2,scenesNumber); //Canviar el primer num si fem més menus
        
            

            if(int.Parse(GameObject.FindGameObjectWithTag(llistaInfinit[i]).getTag) == contadorDificultat){
                llistaDificultatActual.Add(llistaInfinit[i]);
            }
        }
        
        int sizeOfList;
        sizeOfList = llistaDificultatActual.Count;

        int randomNum = UnityEngine.Random.Range(0,sizeOfList - 1);
        Debug.Log(randomNum);

        string scene = llistaDificultatActual[randomNum];
        SceneManager.LoadScene(scene);
        */

    }

    public void PrimerNivell (){
        
        infinit = true;
        PassarEscenaInfinit();
    }
}
