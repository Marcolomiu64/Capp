﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraTemps : MonoBehaviour
{

    Image timerBar;
    public float maxTime = 5f;
    public static float timeLeft;
    //public GameObject objecte_scripts;
    
    //Inactive
    public GameObject PopUp;
    public GameObject Continuar;
    public GameObject TornarInici;
    //Active
    public GameObject PararTemps_Button;
    public GameObject Sol1;
    public GameObject Sol2;
    public GameObject Sol3;
    public GameObject Sol4;
    
    void Awake(){

        timerBar = GetComponent<Image>();
        timeLeft = maxTime;
    }

    // Update is called once per frame
    void Update(){

        if (timeLeft > 0){
            timeLeft -= Time.deltaTime;
            timerBar.fillAmount = timeLeft / maxTime;
        }
        
        else if (timeLeft == -13){  //Valor designat per quan s'apreti el botó de parar el temps

            timerBar.fillAmount = timerBar.fillAmount;
        }

        else{
            /* if si estas als nivell de dificultat 9
            if(){
                objecte_scripts.GetComponent<Answer>().IncorrectAnswerLvl10();  
            }
            */
            PopUp.SetActive(true);
            Continuar.SetActive(true);
            TornarInici.SetActive(true);

            PararTemps_Button.SetActive(false);
            
            if(Sol1 != null){
                Sol1.SetActive(false);
            }

            if(Sol2 != null){
                Sol2.SetActive(false);
            }
            
            if(Sol3 != null){
                Sol3.SetActive(false);
            }
            
            if(Sol4 != null){
                Sol4.SetActive(false);
            }
            
            //objecte_scripts.GetComponent<Answer>().IncorrectAnswer();
        }
    }
}
