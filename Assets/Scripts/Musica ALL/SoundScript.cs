﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundScript : MonoBehaviour
{
    private DontDestroyAudio dontDestroyAudio;
    public Button musicToggleButton;
    public Sprite musicOn;
    public Sprite musicOff;

    void Start()
    {
        dontDestroyAudio = GameObject.FindObjectOfType<DontDestroyAudio>();
        UpdateIconVolume();
    }

    
    void Update()
    {
        
    }

    public void PauseMusic(){

        dontDestroyAudio.ToggleSound();
        UpdateIconVolume();
    }

    void UpdateIconVolume(){

        if(PlayerPrefs.GetInt("Muted",0) == 0){
            AudioListener.volume = 1;
            musicToggleButton.GetComponent<Image>().sprite = musicOn;
        }else{
            AudioListener.volume = 0;
            musicToggleButton.GetComponent<Image>().sprite = musicOff;

        }
    }
}
