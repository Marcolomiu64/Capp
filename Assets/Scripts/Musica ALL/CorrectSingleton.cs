﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CorrectSingleton : MonoBehaviour
{
    static CorrectSingleton instance = null;

    public void Awake(){
        
        if (instance != null){
            Destroy(instance);
        }
            
        instance = this;
        DontDestroyOnLoad(transform.gameObject);
        
        /*
        if(EffectsScript.effectsBool == true){

            if (instance != null){
                Destroy(gameObject);
            }else{
                instance = this;
                DontDestroyOnLoad(transform.gameObject);
            }
            //Destroy(gameObject,1);
        }*/
    }
}
