﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyAudio : MonoBehaviour
{
    static DontDestroyAudio instance = null;

    public void Awake(){

        if (instance != null){
            Destroy(gameObject);
        }else{
            instance = this;
            DontDestroyOnLoad(transform.gameObject);
        }
    }

    public void ToggleSound(){

        if (PlayerPrefs.GetInt("Muted", 0) == 0){
            PlayerPrefs.SetInt("Muted", 1);
            //AudioListener.volume = 1;
        }else{
            PlayerPrefs.SetInt("Muted", 0);
            //AudioListener.volume = 0;
        }
    }
}
