﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectsScript : MonoBehaviour
{
    public Button effectToggleButton;
    public Sprite effectsOn;
    public Sprite effectsOff;

    public static bool effectsBool;

    void Start(){
        UpdateIconEffects();
    }

    // Update is called once per frame
    void Update(){
        
    }

    public void PauseEffects(){

        ToggleEffects();
        UpdateIconEffects();
    }

    void UpdateIconEffects(){

        if(PlayerPrefs.GetInt("Effects Muted",0) == 0){
            effectToggleButton.GetComponent<Image>().sprite = effectsOn;
            effectsBool = true;
        }else{
            effectToggleButton.GetComponent<Image>().sprite = effectsOff;
            effectsBool = false;
        }
    }

    public void ToggleEffects(){

        if (PlayerPrefs.GetInt("Effects Muted", 0) == 0){
            PlayerPrefs.SetInt("Effects Muted", 1);
            effectsBool = true;
        }else{
            PlayerPrefs.SetInt("Effects Muted", 0);
            effectsBool = false;            
        }
    }
}
