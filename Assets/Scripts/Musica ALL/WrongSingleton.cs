﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrongSingleton : MonoBehaviour
{
    static WrongSingleton instance = null;

    public void Awake(){

        if (instance != null){
            Destroy(instance);
        }
            
        instance = this;
        DontDestroyOnLoad(transform.gameObject);
    }
}