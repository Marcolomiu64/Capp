# Capp - Projecte de Síntesi

## Tutor: Marc Vives

## Integrants del grup: Marc Argilés Coloma, David Giménez Rodríguez

Capp és un joc que combina diferents puzzles per afavorir la concentració del/la jugador/a

### Mecànica de joc: Troba la resposta abans que acabi el temps, la dificultat va en augment

## Modes de joc
### Diari

10 nivells aleatoris al dia (un de cada dificultat), es pot fer quantes vegades el vulgui

### Infinit (Botó play)

Jugaràs nivells sense parar fins a fallar 3 vegades, la dificultat puja si encadenes 5 respostes correctes

### Minijoc 1: Matemàtiques

Fes click al botó que fa complir l'operació matemàtica

### Minijoc 2: Cap a on gira l'engranatge assenyalat?

Troba el sentit en el que gira l'engranatge assenyalat

### Minijoc 3: Com continua la sèrie?

Troba la següent figura que segueix la sèrie d'una manera lògica

### Minijoc 4: Quina silueta correspon a la imatge superior?

Es mostra una imatge a la part superior de la pantalla i s'ha de trobar la silueta que encaixa amb la imatge

### Moneda: Punts

Obtens 200 punts per resposta encertada i pots parar el temps gastant 1000 punts